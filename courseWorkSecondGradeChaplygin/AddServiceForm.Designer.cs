﻿
namespace courseWorkSecondGradeChaplygin
{
    partial class AddServiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TitleInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DescriptionInput = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CostInput = new System.Windows.Forms.NumericUpDown();
            this.ExecutionTimeInput = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.SubmitButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.CostInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExecutionTimeInput)).BeginInit();
            this.SuspendLayout();
            // 
            // TitleInput
            // 
            this.TitleInput.Location = new System.Drawing.Point(40, 75);
            this.TitleInput.Name = "TitleInput";
            this.TitleInput.Size = new System.Drawing.Size(247, 26);
            this.TitleInput.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Название";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Описание";
            // 
            // DescriptionInput
            // 
            this.DescriptionInput.AcceptsReturn = true;
            this.DescriptionInput.AcceptsTab = true;
            this.DescriptionInput.Location = new System.Drawing.Point(40, 180);
            this.DescriptionInput.Multiline = true;
            this.DescriptionInput.Name = "DescriptionInput";
            this.DescriptionInput.Size = new System.Drawing.Size(247, 144);
            this.DescriptionInput.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(478, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Стоимость";
            // 
            // CostInput
            // 
            this.CostInput.Location = new System.Drawing.Point(482, 76);
            this.CostInput.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.CostInput.Name = "CostInput";
            this.CostInput.Size = new System.Drawing.Size(214, 26);
            this.CostInput.TabIndex = 7;
            // 
            // ExecutionTimeInput
            // 
            this.ExecutionTimeInput.Location = new System.Drawing.Point(482, 181);
            this.ExecutionTimeInput.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.ExecutionTimeInput.Name = "ExecutionTimeInput";
            this.ExecutionTimeInput.Size = new System.Drawing.Size(214, 26);
            this.ExecutionTimeInput.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(478, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(207, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Время выполнения услуги";
            // 
            // SubmitButton
            // 
            this.SubmitButton.Location = new System.Drawing.Point(482, 274);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(203, 49);
            this.SubmitButton.TabIndex = 10;
            this.SubmitButton.Text = "Добавить";
            this.SubmitButton.UseVisualStyleBackColor = true;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // AddServiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 475);
            this.Controls.Add(this.SubmitButton);
            this.Controls.Add(this.ExecutionTimeInput);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CostInput);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DescriptionInput);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TitleInput);
            this.Name = "AddServiceForm";
            this.Text = "AddServiceForm";
            this.Load += new System.EventHandler(this.AddServiceForm_Load);
            this.Controls.SetChildIndex(this.TitleInput, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.DescriptionInput, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.CostInput, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.ExecutionTimeInput, 0);
            this.Controls.SetChildIndex(this.SubmitButton, 0);
            ((System.ComponentModel.ISupportInitialize)(this.CostInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExecutionTimeInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TitleInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox DescriptionInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown CostInput;
        private System.Windows.Forms.NumericUpDown ExecutionTimeInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button SubmitButton;
    }
}