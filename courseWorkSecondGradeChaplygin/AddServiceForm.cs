﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class AddServiceForm : TemplateForm
    {
        public AddServiceForm()
        {
            InitializeComponent();
        }

        private void AddServiceForm_Load(object sender, EventArgs e)
        {

        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            string title = TitleInput.Text.Trim();
            string description = DescriptionInput.Text.Trim();
            int cost = Convert.ToInt32(CostInput.Value);
            int executionTime = Convert.ToInt32(ExecutionTimeInput.Value);

            if(title != "" && description != "" && cost >= 0 && executionTime > 0)
            {
                Service newService = new Service();
                newService.title = title;
                newService.description = description;
                newService.executionTime = executionTime;
                newService.cost = cost;

                DataBaseController dbController = new DataBaseController();
                dbController.AddService(newService);
                this.Close();
            }
        }
    }
}
