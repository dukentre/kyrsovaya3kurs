﻿
namespace courseWorkSecondGradeChaplygin
{
    partial class AdminUserRegistrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FullNameInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LoginInput = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PasswordInput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.UserRoleCombobox = new System.Windows.Forms.ComboBox();
            this.IsRegularCustomerCheckbox = new System.Windows.Forms.CheckBox();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // FullNameInput
            // 
            this.FullNameInput.Location = new System.Drawing.Point(38, 80);
            this.FullNameInput.Name = "FullNameInput";
            this.FullNameInput.Size = new System.Drawing.Size(235, 26);
            this.FullNameInput.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "ФИО";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Логин";
            // 
            // LoginInput
            // 
            this.LoginInput.Location = new System.Drawing.Point(38, 171);
            this.LoginInput.Name = "LoginInput";
            this.LoginInput.Size = new System.Drawing.Size(235, 26);
            this.LoginInput.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 224);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Пароль";
            // 
            // PasswordInput
            // 
            this.PasswordInput.Location = new System.Drawing.Point(38, 267);
            this.PasswordInput.Name = "PasswordInput";
            this.PasswordInput.Size = new System.Drawing.Size(235, 26);
            this.PasswordInput.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(525, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Роль";
            // 
            // UserRoleCombobox
            // 
            this.UserRoleCombobox.FormattingEnabled = true;
            this.UserRoleCombobox.Items.AddRange(new object[] {
            "client",
            "master",
            "admin"});
            this.UserRoleCombobox.Location = new System.Drawing.Point(529, 80);
            this.UserRoleCombobox.Name = "UserRoleCombobox";
            this.UserRoleCombobox.Size = new System.Drawing.Size(231, 28);
            this.UserRoleCombobox.TabIndex = 9;
            // 
            // IsRegularCustomerCheckbox
            // 
            this.IsRegularCustomerCheckbox.AutoSize = true;
            this.IsRegularCustomerCheckbox.Location = new System.Drawing.Point(529, 171);
            this.IsRegularCustomerCheckbox.Name = "IsRegularCustomerCheckbox";
            this.IsRegularCustomerCheckbox.Size = new System.Drawing.Size(221, 24);
            this.IsRegularCustomerCheckbox.TabIndex = 10;
            this.IsRegularCustomerCheckbox.Text = "Постоянный покупатель";
            this.IsRegularCustomerCheckbox.UseVisualStyleBackColor = true;
            // 
            // SubmitButton
            // 
            this.SubmitButton.Location = new System.Drawing.Point(529, 258);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(221, 44);
            this.SubmitButton.TabIndex = 11;
            this.SubmitButton.Text = "Зарегистрировать";
            this.SubmitButton.UseVisualStyleBackColor = true;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // AdminUserRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.SubmitButton);
            this.Controls.Add(this.IsRegularCustomerCheckbox);
            this.Controls.Add(this.UserRoleCombobox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PasswordInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LoginInput);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FullNameInput);
            this.Name = "AdminUserRegistration";
            this.Text = "AdminUserRegistration";
            this.Load += new System.EventHandler(this.AdminUserRegistration_Load);
            this.Controls.SetChildIndex(this.FullNameInput, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.LoginInput, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.PasswordInput, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.UserRoleCombobox, 0);
            this.Controls.SetChildIndex(this.IsRegularCustomerCheckbox, 0);
            this.Controls.SetChildIndex(this.SubmitButton, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox FullNameInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox LoginInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PasswordInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox UserRoleCombobox;
        private System.Windows.Forms.CheckBox IsRegularCustomerCheckbox;
        private System.Windows.Forms.Button SubmitButton;
    }
}