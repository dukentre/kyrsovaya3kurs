﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class AdminUserRegistrationForm : TemplateForm
    {
        public AdminUserRegistrationForm()
        {
            InitializeComponent();
        }

        private void AdminUserRegistration_Load(object sender, EventArgs e)
        {

        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            string fullName = FullNameInput.Text.Trim();
            string login = LoginInput.Text.Trim();
            string password = PasswordInput.Text.Trim();
            string userRole = "";
            if(UserRoleCombobox.SelectedItem != null)
            {
                userRole = UserRoleCombobox.SelectedItem.ToString().Trim();
            }
            bool isRegularCustomer = IsRegularCustomerCheckbox.Checked;
            if(fullName != "" && login != "" && password != "" && userRole != "")
            {
                User newUser = new User();
                newUser.fullName = fullName;
                newUser.login = login;
                newUser.password = password;
                newUser.userRole = User.getRoleByName(userRole);
                newUser.isRegularCustomer = isRegularCustomer;

                DataBaseController dbController = new DataBaseController();
                dbController.AddUser(newUser);

                this.Close();
            }
        }
    }
}
