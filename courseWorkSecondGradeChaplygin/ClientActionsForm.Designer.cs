﻿
namespace courseWorkSecondGradeChaplygin
{
    partial class ClientActionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AuthButton = new System.Windows.Forms.Button();
            this.RegButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AuthButton
            // 
            this.AuthButton.Location = new System.Drawing.Point(97, 154);
            this.AuthButton.Name = "AuthButton";
            this.AuthButton.Size = new System.Drawing.Size(226, 57);
            this.AuthButton.TabIndex = 1;
            this.AuthButton.Text = "У меня есть аккаунт";
            this.AuthButton.UseVisualStyleBackColor = true;
            this.AuthButton.Click += new System.EventHandler(this.AuthButton_Click);
            // 
            // RegButton
            // 
            this.RegButton.Location = new System.Drawing.Point(97, 251);
            this.RegButton.Name = "RegButton";
            this.RegButton.Size = new System.Drawing.Size(226, 57);
            this.RegButton.TabIndex = 2;
            this.RegButton.Text = "Зарегистрироваться";
            this.RegButton.UseVisualStyleBackColor = true;
            this.RegButton.Click += new System.EventHandler(this.RegButton_Click);
            // 
            // ClientActionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 504);
            this.Controls.Add(this.RegButton);
            this.Controls.Add(this.AuthButton);
            this.Name = "ClientActionsForm";
            this.Text = "ClientActionsForms";
            this.Load += new System.EventHandler(this.ClientActionsForms_Load);
            this.Controls.SetChildIndex(this.AuthButton, 0);
            this.Controls.SetChildIndex(this.RegButton, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button AuthButton;
        private System.Windows.Forms.Button RegButton;
    }
}