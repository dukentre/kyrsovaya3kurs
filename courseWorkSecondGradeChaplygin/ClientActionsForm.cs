﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class ClientActionsForm : TemplateForm
    {
        public ClientActionsForm()
        {
            InitializeComponent();
        }

        private void ClientActionsForms_Load(object sender, EventArgs e)
        {

        }

        private void AuthButton_Click(object sender, EventArgs e)
        {
            ClientAuthorizationForm form = new ClientAuthorizationForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }

        private void RegButton_Click(object sender, EventArgs e)
        {
            ClientRegistrationForm form = new ClientRegistrationForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }
    }
}
