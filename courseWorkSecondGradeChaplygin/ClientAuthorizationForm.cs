﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class ClientAuthorizationForm : TemplateForm
    {
        public ClientAuthorizationForm()
        {
            InitializeComponent();
        }

        private void ClientAuthorizationForm_Load(object sender, EventArgs e)
        {

        }

        private void EnterButton_Click(object sender, EventArgs e)
        {
            DataBaseController dbController = new DataBaseController();

            string login = PhoneNumberInput.Text.Trim();
            string password = PasswordInput.Text.Trim();
            if (login != "" && password != "")
            {
                try
                {
                    dbController.AuthorizateUser(login, password, false);
                    UserMenuForm form = new UserMenuForm();
                    form.Owner = this.Owner;
                    form.Show();
                    this.Owner = null;
                    this.Close();
                }
                catch
                {
                    MessageBox.Show("Неверный логин или пароль");
                    PasswordInput.Text = "";
                }
            }
        }
    }
}
