﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class ClientRegistrationForm : TemplateForm
    {
        public ClientRegistrationForm()
        {
            InitializeComponent();
        }

        private void ClientRegistrationForm_Load(object sender, EventArgs e)
        {

        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            long phoneNumber;
            string fullName = FullNameInput.Text.Trim();
            if(fullName.Length < 4)
            {
                MessageBox.Show("Введите корректное ФИО");
                return;
            }
            try
            {
                phoneNumber = Convert.ToInt64(PhoneNumberInput.Text.Trim());
                if(phoneNumber.ToString().Length != 11)
                {
                    MessageBox.Show("Введите корректный номер телефона");
                    return;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Введите корректный номер телефона");
                return;
            }
            string password = PasswordInput.Text.Trim();
            if (password.Length < 6)
            {
                MessageBox.Show("Введите безопасный пароль");
                return;
            }
            string repeatPassword = RepeatPasswordInput.Text.Trim();
            if(password != repeatPassword)
            {
                MessageBox.Show("Пароли не совпадают");
                return;
            }

            if (fullName != "" && phoneNumber.ToString() != "" && password != "")
            {
                DataBaseController dbController = new DataBaseController();
                if(dbController.getUserByLogin(phoneNumber.ToString()) != null)
                {
                    MessageBox.Show("Пользователь с данным номером телефона уже существует");
                    return;
                }
                dbController.RegistrateUser(fullName, phoneNumber.ToString(), password);
                UserMenuForm form = new UserMenuForm();
                form.Owner = this.Owner;
                form.Show();
                this.Owner = null;
                this.Close();
            }
        }
    }
}
