﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace courseWorkSecondGradeChaplygin.DataBase
{
    class DataBaseController
    {

        static List<User> users = new List<User>();
        static List<Service> services = new List<Service>();
        static List<Order> orders = new List<Order>();

        private static int lastUserId;
        private static int lastServiceId;
        private static int lastOrderId;

        const string usersDBFilePath = @".\users.json";
        const string servicesDBFilePath = @".\services.json";
        const string ordersDBFilePath = @".\orders.json";

        public void LoadDataBase()
        {
            repairDataBase();

            // чтение из файла
            using (FileStream fstream = File.OpenRead(usersDBFilePath))
            {
                // преобразуем строку в байты
                byte[] array = new byte[fstream.Length];
                // считываем данные
                fstream.Read(array, 0, array.Length);
                // декодируем байты в строку
                string textFromFile = System.Text.Encoding.UTF8.GetString(array);

                users = JsonConvert.DeserializeObject<List<User>>(textFromFile);

                int maxIndex = 0;
                for(int i =0; i<users.Count; i++)
                {
                    if(maxIndex < users[i].id)
                    {
                        maxIndex = users[i].id;
                    }
                }
                lastUserId = maxIndex;

            }

            // чтение из файла
            using (FileStream fstream = File.OpenRead(servicesDBFilePath))
            {
                // преобразуем строку в байты
                byte[] array = new byte[fstream.Length];
                // считываем данные
                fstream.Read(array, 0, array.Length);
                // декодируем байты в строку
                string textFromFile = System.Text.Encoding.UTF8.GetString(array);

                services = JsonConvert.DeserializeObject<List<Service>>(textFromFile);

                int maxIndex = 0;
                for (int i = 0; i < services.Count; i++)
                {
                    if (maxIndex < services[i].id)
                    {
                        maxIndex = services[i].id;
                    }
                }
                lastServiceId = maxIndex;
            }

            // чтение из файла
            using (FileStream fstream = File.OpenRead(ordersDBFilePath))
            {
                // преобразуем строку в байты
                byte[] array = new byte[fstream.Length];
                // считываем данные
                fstream.Read(array, 0, array.Length);
                // декодируем байты в строку
                string textFromFile = System.Text.Encoding.UTF8.GetString(array);

                orders = JsonConvert.DeserializeObject<List<Order>>(textFromFile);

                int maxIndex = 0;
                for (int i = 0; i < orders.Count; i++)
                {
                    if (maxIndex < orders[i].id)
                    {
                        maxIndex = orders[i].id;
                    }
                }
                lastOrderId = maxIndex;

            }
        }


        private void repairDataBase()
        {
            FileInfo fileInf = new FileInfo(usersDBFilePath);
            if (!fileInf.Exists)
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Converters.Add(new JavaScriptDateTimeConverter());
                serializer.NullValueHandling = NullValueHandling.Ignore;

                List<User> users = new List<User>();

                User mainAdmin = new User();
                mainAdmin.id = 1;
                mainAdmin.fullName = "Chaplygin Artem Eduardovich";
                mainAdmin.login = "dukentre";
                mainAdmin.password = "qwerty123";
                mainAdmin.userRole = User.role.admin;

                users.Add(mainAdmin);

                using (StreamWriter sw = new StreamWriter(usersDBFilePath))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, users);
                }
            }

            FileInfo servicesFileInf = new FileInfo(servicesDBFilePath);
            if (!servicesFileInf.Exists)
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Converters.Add(new JavaScriptDateTimeConverter());
                serializer.NullValueHandling = NullValueHandling.Ignore;

                List<Service> services = new List<Service>();


                using (StreamWriter sw = new StreamWriter(servicesDBFilePath))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, services);
                }
            }
            FileInfo ordersFileInf = new FileInfo(ordersDBFilePath);
            if (!ordersFileInf.Exists)
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Converters.Add(new JavaScriptDateTimeConverter());
                serializer.NullValueHandling = NullValueHandling.Ignore;

                List<Order> orders = new List<Order>();


                using (StreamWriter sw = new StreamWriter(ordersDBFilePath))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, orders);
                }
            }
        }


        public void SaveDataBase()
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;

            using (StreamWriter sw = new StreamWriter(usersDBFilePath))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, users.ToArray());
            }

            using (StreamWriter sw = new StreamWriter(servicesDBFilePath))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, services.ToArray());
            }

            using (StreamWriter sw = new StreamWriter(ordersDBFilePath))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, orders.ToArray());
            }
        }

        public void RegistrateUser(string fullName, string phoneNumber, string password)
        {
            User newUser = new User();
            newUser.id = ++lastUserId;
            newUser.fullName = fullName;
            newUser.login = phoneNumber;
            newUser.password = password;
            newUser.userRole = User.role.client;

            users.Add(newUser);

            SaveDataBase();

            ProgramState pState = new ProgramState();
            pState.setAuthorizedUser(newUser);
        }

        public void AuthorizateUser(string login, string password,bool isWorker)
        {
            ProgramState pState = new ProgramState();
            for(int i=0; i<users.Count; i++)
            {
                if(users[i].login == login && users[i].password == password && ( (isWorker && (users[i].userRole == User.role.master || users[i].userRole == User.role.admin) || (!isWorker && users[i].userRole == User.role.client) ) ))
                {
                    pState.setAuthorizedUser(users[i]);
                    return;
                }
            }

            throw new Exception("USER_NOT_FOUND");
        }

        public List<User> getUsers()
        {
            return users;
        }

        public void UpdateUser(int userId,User newUser)
        {
            users[userId] = newUser;
            SaveDataBase();
        }

        public void DeleteUser(int userId)
        {
            users.RemoveAt(userId);
            SaveDataBase();
        }

        public void AddUser(User newUser)
        {
            newUser.id = ++lastUserId;
            users.Add(newUser);
            SaveDataBase();
        }


        public void AddService(Service newService)
        {
            newService.id = ++lastServiceId;
            services.Add(newService);
            SaveDataBase();
        }

        public List<Service> GetServices()
        {
            return services;
        }

        public void UpdateService(int serviceId, Service newService)
        {
            services[serviceId] = newService;
            SaveDataBase();
        }

        public void DeleteService(int serviceId)
        {
            services.RemoveAt(serviceId);
            SaveDataBase();
        }

        public Service getServiceById(int serviceId)
        {
            for(int i = 0; i<services.Count; i++)
            {
                if(services[i].id == serviceId)
                {
                    return services[i];
                }
            }
            return null;
        }

        public bool AddOrder(Order newOrder)
        {
            newOrder.id = ++lastOrderId;
            Service newService = getServiceById(newOrder.serviceId);
            if(newService == null)
            {
                return false;
            }

            DateTime newOrderDate = new DateTime(newOrder.serviceDate);
            DateTime newOrderTime = new DateTime(newOrder.serviceTime);

            DateTime newOrderTimeStart = new DateTime(newOrderDate.Year, newOrderDate.Month, newOrderDate.Day, newOrderTime.Hour, newOrderTime.Minute, newOrderTime.Second);
            DateTime newOrderTimeEnd = newOrderTime.AddMinutes(newService.executionTime);

            for(int i =0; i<orders.Count; i++)
            {
                Service service = getServiceById(orders[i].serviceId);
                if (service == null)
                {
                    continue;
                }
                DateTime orderDate = new DateTime(orders[i].serviceDate);
                DateTime orderTime = new DateTime(orders[i].serviceTime);

                DateTime orderTimeStart = new DateTime(orderDate.Year, orderDate.Month, orderDate.Day, orderTime.Hour, orderTime.Minute, orderTime.Second);
                DateTime orderTimeEnd = orderTime.AddMinutes(service.executionTime+5);

                if((newOrderTimeStart > orderTimeStart && newOrderTimeStart < orderTimeEnd) || (newOrderTimeEnd > orderTimeStart && newOrderTimeEnd < orderTimeEnd))
                {
                    return false;
                }
            }

            orders.Add(newOrder);
            SaveDataBase();

            return true;
        }

        public List<Order> GetOrders()
        {
            return orders;
        }

        public User getUserById(int userId)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].id == userId)
                {
                    return users[i];
                }
            }
            return null;
        }
        public User getUserByLogin(string login)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].login == login)
                {
                    return users[i];
                }
            }
            return null;
        }

        public void UpdateOrderStatus(int orderId, Order.Statuses newStatus)
        {
            orders[orderId].status = newStatus;
            SaveDataBase();
        }
    }
}
