﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace courseWorkSecondGradeChaplygin.DataBase
{
    class Order
    {
        public int id;
        public int serviceId;
        public int userId;
        public long serviceDate;
        public long serviceTime;
        public Statuses status = Statuses.inPlan;

        public enum Statuses
        {
            inPlan,
            inProgress,
            completed,
            canceled,
        }

        public static string getStatusName(Statuses status)
        {
            switch (status)
            {
                case Statuses.inProgress:
                    return "inProgress";
                case Statuses.completed:
                    return "completed";
                case Statuses.canceled:
                    return "canceled";
                case Statuses.inPlan:
                default:
                    return "inPlan";

            }
        }

        public static Statuses getStatusByName(string statusName)
        {
            switch (statusName)
            {
                case "inProgress":
                    return Statuses.inProgress;
                case "completed":
                    return Statuses.completed;
                case "canceled":
                    return Statuses.canceled;
                case "inPlan":
                default:
                    return Statuses.inPlan;

            }
        }
    }
}
