﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace courseWorkSecondGradeChaplygin.DataBase
{
    class User
    {
        public int id;
        public string fullName;
        public string login;
        public string password;
        public role userRole;
        public bool isRegularCustomer = false;
        public enum role {
            client,
            admin,
            master
        }


        static public string getRoleName(role role)
        {
            switch (role)
            {
                case role.client:
                    return "client";
                case role.master:
                    return "master";
                case role.admin:
                    return "admin";
                default:
                    return "client";
            }
        }

        static public role getRoleByName(string roleName)
        {
            switch (roleName)
            {
                case "client":
                    return role.client;
                case "master":
                    return role.master;
                case "admin":
                    return role.admin;
                default:
                    return role.client;
            }
        }
    }
}
