﻿
namespace courseWorkSecondGradeChaplygin
{
    partial class MainMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IAmClientButton = new System.Windows.Forms.Button();
            this.IAmWorkerButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // IAmClientButton
            // 
            this.IAmClientButton.Location = new System.Drawing.Point(85, 167);
            this.IAmClientButton.Name = "IAmClientButton";
            this.IAmClientButton.Size = new System.Drawing.Size(168, 50);
            this.IAmClientButton.TabIndex = 1;
            this.IAmClientButton.Text = "Я клиент";
            this.IAmClientButton.UseVisualStyleBackColor = true;
            this.IAmClientButton.Click += new System.EventHandler(this.IAmClientButton_Click);
            // 
            // IAmWorkerButton
            // 
            this.IAmWorkerButton.Location = new System.Drawing.Point(85, 259);
            this.IAmWorkerButton.Name = "IAmWorkerButton";
            this.IAmWorkerButton.Size = new System.Drawing.Size(168, 50);
            this.IAmWorkerButton.TabIndex = 2;
            this.IAmWorkerButton.Text = "Я работник";
            this.IAmWorkerButton.UseVisualStyleBackColor = true;
            this.IAmWorkerButton.Click += new System.EventHandler(this.IAmWorkerButton_Click);
            // 
            // MainMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 532);
            this.Controls.Add(this.IAmWorkerButton);
            this.Controls.Add(this.IAmClientButton);
            this.Name = "MainMenuForm";
            this.Text = "Главное меню";
            this.Load += new System.EventHandler(this.MainMenuForm_Load);
            this.Controls.SetChildIndex(this.IAmClientButton, 0);
            this.Controls.SetChildIndex(this.IAmWorkerButton, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button IAmClientButton;
        private System.Windows.Forms.Button IAmWorkerButton;
    }
}