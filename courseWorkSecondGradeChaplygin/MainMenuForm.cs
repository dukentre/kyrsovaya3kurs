﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class MainMenuForm : TemplateForm
    {
        public MainMenuForm()
        {
            InitializeComponent();
            DataBaseController dbController = new DataBaseController();
            dbController.LoadDataBase();
        }

        private void MainMenuForm_Load(object sender, EventArgs e)
        {

        }

        private void IAmClientButton_Click(object sender, EventArgs e)
        {
            ClientActionsForm form = new ClientActionsForm();
            form.Show();
            this.Hide();
            form.Owner = this;
        }

        private void IAmWorkerButton_Click(object sender, EventArgs e)
        {
            WorkerAuthorizationForm form = new WorkerAuthorizationForm();
            form.Show();
            this.Hide();
            form.Owner = this;
        }
    }
}
