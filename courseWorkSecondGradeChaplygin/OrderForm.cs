﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class OrderForm : TemplateForm
    {
        public OrderForm()
        {
            InitializeComponent();
        }

        private void OrderForm_Load(object sender, EventArgs e)
        {
            DataBaseController dbController = new DataBaseController();
            List<Service> services = dbController.GetServices();
            for(int i = 0; i<services.Count; i++)
            {
                servicesList.Items.Add(services[i]);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            if(servicesList.SelectedIndex < 0)
            {
                return;
            }

            ProgramState pState = new ProgramState();

            int userId = pState.GetAuthorizedUser().id;
            int serviceId = ((Service)(servicesList.SelectedItem)).id;
            long date = dateInput.Value.Ticks;
            long time = timeInput.Value.Ticks;

            Order newOrder = new Order();

            newOrder.userId = userId;
            newOrder.serviceId = serviceId;
            newOrder.serviceDate = date;
            newOrder.serviceTime = time;

            DataBaseController dbController = new DataBaseController();
            bool isSuccessfully = dbController.AddOrder(newOrder);
            if (isSuccessfully)
            {
                this.Close();
                MessageBox.Show("Вы были перенаправлены на платёжный шлюз");
            }
            else
            {
                MessageBox.Show("Владелец будет занят в это время.\nВыберите другое.");
            }
        }

        private void servicesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Service service = (Service) servicesList.SelectedItem;

            serviceDescription.Text = service.description;
            cost.Value = service.cost;
            executionTime.Value = service.executionTime;
        }
    }
}
