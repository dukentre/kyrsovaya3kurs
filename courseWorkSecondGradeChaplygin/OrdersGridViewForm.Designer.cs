﻿
namespace courseWorkSecondGradeChaplygin
{
    partial class OrdersGridViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OrdersGridView = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviceId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sevice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.print = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // OrdersGridView
            // 
            this.OrdersGridView.AllowUserToAddRows = false;
            this.OrdersGridView.AllowUserToDeleteRows = false;
            this.OrdersGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrdersGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.serviceId,
            this.sevice,
            this.customerFullName,
            this.orderTime,
            this.status,
            this.print});
            this.OrdersGridView.Location = new System.Drawing.Point(13, 13);
            this.OrdersGridView.Name = "OrdersGridView";
            this.OrdersGridView.RowHeadersWidth = 62;
            this.OrdersGridView.RowTemplate.Height = 28;
            this.OrdersGridView.Size = new System.Drawing.Size(775, 425);
            this.OrdersGridView.TabIndex = 1;
            this.OrdersGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.OrdersGridView_CellMouseClick);
            this.OrdersGridView.CellStateChanged += new System.Windows.Forms.DataGridViewCellStateChangedEventHandler(this.OrdersGridView_CellStateChanged);
            // 
            // id
            // 
            this.id.HeaderText = "Id";
            this.id.MinimumWidth = 8;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Width = 150;
            // 
            // serviceId
            // 
            this.serviceId.HeaderText = "Id сервиса";
            this.serviceId.MinimumWidth = 8;
            this.serviceId.Name = "serviceId";
            this.serviceId.ReadOnly = true;
            this.serviceId.Width = 150;
            // 
            // sevice
            // 
            this.sevice.HeaderText = "Сервис";
            this.sevice.MinimumWidth = 8;
            this.sevice.Name = "sevice";
            this.sevice.ReadOnly = true;
            this.sevice.Width = 150;
            // 
            // customerFullName
            // 
            this.customerFullName.HeaderText = "ФИО покупателя";
            this.customerFullName.MinimumWidth = 8;
            this.customerFullName.Name = "customerFullName";
            this.customerFullName.ReadOnly = true;
            this.customerFullName.Width = 150;
            // 
            // orderTime
            // 
            this.orderTime.HeaderText = "Время начала заказа";
            this.orderTime.MinimumWidth = 8;
            this.orderTime.Name = "orderTime";
            this.orderTime.ReadOnly = true;
            this.orderTime.Width = 150;
            // 
            // status
            // 
            this.status.HeaderText = "Статус заказа";
            this.status.Items.AddRange(new object[] {
            "inPlan",
            "inProgress",
            "completed",
            "canceled"});
            this.status.MinimumWidth = 8;
            this.status.Name = "status";
            this.status.ReadOnly = true;
            this.status.Width = 150;
            // 
            // print
            // 
            this.print.HeaderText = "Отчёт";
            this.print.MinimumWidth = 8;
            this.print.Name = "print";
            this.print.Text = "Отчёт";
            this.print.Width = 150;
            // 
            // OrdersGridViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.OrdersGridView);
            this.Name = "OrdersGridViewForm";
            this.Text = "OrdersGridViewForm";
            this.Load += new System.EventHandler(this.OrdersGridViewForm_Load);
            this.Controls.SetChildIndex(this.OrdersGridView, 0);
            ((System.ComponentModel.ISupportInitialize)(this.OrdersGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView OrdersGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn serviceId;
        private System.Windows.Forms.DataGridViewTextBoxColumn sevice;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderTime;
        private System.Windows.Forms.DataGridViewComboBoxColumn status;
        private System.Windows.Forms.DataGridViewButtonColumn print;
    }
}