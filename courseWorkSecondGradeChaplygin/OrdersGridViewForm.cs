﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class OrdersGridViewForm : TemplateForm
    {

        bool isRowEdit = false;
        int rowEditNumber = -1;

        bool isAdminOrMaster = false;

        public OrdersGridViewForm()
        {
            InitializeComponent();

            ProgramState pState = new ProgramState();
            User user = pState.GetAuthorizedUser();
            isAdminOrMaster = user.userRole == User.role.admin || user.userRole == User.role.master;

            if (isAdminOrMaster)
            {
                status.ReadOnly = false;
            }
        }

        private void OrdersGridViewForm_Load(object sender, EventArgs e)
        {
            loadAndDisplayUsersFromDB();
        }

        private void loadAndDisplayUsersFromDB()
        {
            DataBaseController dbController = new DataBaseController();
            List<Order> orders = dbController.GetOrders();

            displayUsers(orders);
        }
        private void displayUsers(List<Order> orders)
        {
            DataBaseController dbController = new DataBaseController();
            OrdersGridView.Rows.Clear();

            ProgramState pState = new ProgramState();
            User authorizedUser = pState.GetAuthorizedUser();
            for (int i = 0; i < orders.Count; i++)
            {
                try {
                    if (!isAdminOrMaster && orders[i].userId != authorizedUser.id)
                    {
                        continue;
                    }
                    User user = dbController.getUserById(orders[i].userId);
                    Service service = dbController.getServiceById(orders[i].serviceId);
                    DateTime startDate = new DateTime(orders[i].serviceDate);
                    DateTime startTime = new DateTime(orders[i].serviceTime);
                    OrdersGridView.Rows.Add(orders[i].id,service.id, service.title, user == null ? "deleted" : user.fullName, $"{startDate.ToString("dd-MM-yyyy")} {startTime.ToString("HH:mm")}", Order.getStatusName(orders[i].status),"Отчёт");
                } catch (Exception) { }
            }
        }


        private void OrdersGridView_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell.ColumnIndex == 5)
            {
                DataBaseController dbController = new DataBaseController();

                dbController.UpdateOrderStatus(e.Cell.RowIndex, Order.getStatusByName(e.Cell.Value.ToString()));

            }
        }

        private string checkText;
        private void OrdersGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            if(e.ColumnIndex == 6)
            {
                try
                {
                    openReportForm(e.RowIndex);
                }
                catch (Exception)
                {
                    MessageBox.Show("Открыть отчёт не удалось","Ошибка");
                }
            }
        }

        // обработчик события печати
        void PrintPageHandler(object sender, PrintPageEventArgs e)
        {
            // печать строки result
            e.Graphics.DrawString(checkText, new Font("Arial", 14), Brushes.Black, 0, 0);
        }

        private void openReportForm(int rowId)
        {
            DataBaseController dbController = new DataBaseController();
            Service service = dbController.getServiceById(Convert.ToInt32(OrdersGridView.Rows[rowId].Cells[1].Value));

            
            if(service == null)
            {
                throw new Exception();
            }

            ReportCheckForm form = new ReportCheckForm(service);
            form.Owner = this;
            form.Show();
            this.Hide();
           
        }

    }
}
