﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace courseWorkSecondGradeChaplygin
{   
    class ProgramState
    {
        private static User AuthorizedUser;
        
        public void setAuthorizedUser(User user)
        {
            AuthorizedUser = user;
        }

        public void AuthorizedUserLogout()
        {
            AuthorizedUser = null;
        }

        public User GetAuthorizedUser()
        {
            return AuthorizedUser;
        }

    }
}
