﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class ReportCheckForm : TemplateForm
    {
        public ReportCheckForm( Service service)
        {
            InitializeComponent();

            string html = $@"
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
</head>
<body>
<style>
    .title, .address, .list-title, .post-script{{
        margin - left: 18px;
    }}
    div{{
        margin - bottom: 12px;
    }}
    table{{
        border - collapse: collapse;
    }}
    td{{
        border: black solid 1px;
        padding: 8px;
    }}
</style>
<div class='title'>ООО 'Шиномонтажо4ка'</div>
<div class='address'>Г. Курск, ул. Гагарина 27</div>
<div class='list-title'>Список услуг</div>
<table>
    <tbody>
    <tr>
        <td>Наименование</td>
        <td>Стоимость</td>
    </tr>
    <tr>
        <td>{service.title}</td>
        <td>{service.cost}</td>
    </tr>
    </tbody>
</table>
<div class='br'>================================</div>
<div class='total'>Итог: {service.cost} руб.</div>
<div class='post-script'>Спасибо за покупку!</div>
</body>
</html>
";

            webBrowser.DocumentText = html;
           
        }

        private void ReportCheckForm_Load(object sender, EventArgs e)
        {

        }

        private void PrintButton_Click(object sender, EventArgs e)
        {
            webBrowser.ShowPrintDialog();
        }
    }
}
