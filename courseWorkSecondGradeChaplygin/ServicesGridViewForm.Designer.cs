﻿
namespace courseWorkSecondGradeChaplygin
{
    partial class ServicesGridViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServicesGridView = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServiceTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServiceDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServiceCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServiceExecutionTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DeleteButton = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ServicesGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ServicesGridView
            // 
            this.ServicesGridView.AllowUserToAddRows = false;
            this.ServicesGridView.AllowUserToDeleteRows = false;
            this.ServicesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ServicesGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.ServiceTitle,
            this.ServiceDescription,
            this.ServiceCost,
            this.ServiceExecutionTime,
            this.EditButton,
            this.DeleteButton});
            this.ServicesGridView.Location = new System.Drawing.Point(12, 12);
            this.ServicesGridView.Name = "ServicesGridView";
            this.ServicesGridView.RowHeadersWidth = 62;
            this.ServicesGridView.RowTemplate.Height = 28;
            this.ServicesGridView.Size = new System.Drawing.Size(776, 426);
            this.ServicesGridView.TabIndex = 1;
            this.ServicesGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ServicesGridView_CellMouseClick);
            // 
            // id
            // 
            this.id.HeaderText = "Id";
            this.id.MinimumWidth = 8;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Width = 150;
            // 
            // ServiceTitle
            // 
            this.ServiceTitle.HeaderText = "Название услуги";
            this.ServiceTitle.MinimumWidth = 8;
            this.ServiceTitle.Name = "ServiceTitle";
            this.ServiceTitle.ReadOnly = true;
            this.ServiceTitle.Width = 150;
            // 
            // ServiceDescription
            // 
            this.ServiceDescription.HeaderText = "Описание услуги";
            this.ServiceDescription.MinimumWidth = 8;
            this.ServiceDescription.Name = "ServiceDescription";
            this.ServiceDescription.ReadOnly = true;
            this.ServiceDescription.Width = 150;
            // 
            // ServiceCost
            // 
            this.ServiceCost.HeaderText = "Стоимость услуги";
            this.ServiceCost.MinimumWidth = 8;
            this.ServiceCost.Name = "ServiceCost";
            this.ServiceCost.ReadOnly = true;
            this.ServiceCost.Width = 150;
            // 
            // ServiceExecutionTime
            // 
            this.ServiceExecutionTime.HeaderText = "Время выполнения услуги";
            this.ServiceExecutionTime.MinimumWidth = 8;
            this.ServiceExecutionTime.Name = "ServiceExecutionTime";
            this.ServiceExecutionTime.ReadOnly = true;
            this.ServiceExecutionTime.Width = 150;
            // 
            // EditButton
            // 
            this.EditButton.HeaderText = "Редактировать";
            this.EditButton.MinimumWidth = 8;
            this.EditButton.Name = "EditButton";
            this.EditButton.ReadOnly = true;
            this.EditButton.Width = 150;
            // 
            // DeleteButton
            // 
            this.DeleteButton.HeaderText = "Удалить";
            this.DeleteButton.MinimumWidth = 8;
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.ReadOnly = true;
            this.DeleteButton.Width = 150;
            // 
            // ServicesGridViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ServicesGridView);
            this.Name = "ServicesGridViewForm";
            this.Text = "ServicesGridView";
            this.Load += new System.EventHandler(this.ServicesGridView_Load);
            this.Controls.SetChildIndex(this.ServicesGridView, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ServicesGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ServicesGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServiceTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServiceDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServiceCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServiceExecutionTime;
        private System.Windows.Forms.DataGridViewButtonColumn EditButton;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteButton;
    }
}