﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class ServicesGridViewForm : TemplateForm
    {

        bool isRowEdit = false;
        int rowEditNumber = -1;

        public ServicesGridViewForm()
        {
            InitializeComponent();
        }

        private void ServicesGridView_Load(object sender, EventArgs e)
        {
            loadAndDisplayServicesFromDB();
        }

        private void loadAndDisplayServicesFromDB()
        {
            DataBaseController dbController = new DataBaseController();
            List<Service> services = dbController.GetServices();


            displayUsers(services);
        }
        private void displayUsers(List<Service> services)
        {
            ServicesGridView.Rows.Clear();
            for (int i = 0; i < services.Count; i++)
            {
                ServicesGridView.Rows.Add(services[i].id,services[i].title, services[i].description, services[i].cost, services[i].executionTime, "Редактировать", "Удалить");
            }
        }

        private void ServicesGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.RowIndex < 0)
            {
                return;
            }
            if (e.ColumnIndex == 5)
            {
                if (!isRowEdit)
                {
                    startRowEdit(e.RowIndex);
                }
                else
                {
                    if (rowEditNumber == e.RowIndex)
                    {
                        saveRow();
                    }
                    else
                    {
                        saveRow();
                        startRowEdit(e.RowIndex);
                    }
                }
            }
            if (e.ColumnIndex == 6)
            {
                if (MessageBox.Show("Вы уверены, что хотите удалить эти данные?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    deleteRow(e.RowIndex);
            }
        }

        private void startRowEdit(int row)
        {
            isRowEdit = true;
            rowEditNumber = row;
            ServicesGridView.Rows[row].Cells[1].ReadOnly = false;
            ServicesGridView.Rows[row].Cells[2].ReadOnly = false;
            ServicesGridView.Rows[row].Cells[3].ReadOnly = false;
            ServicesGridView.Rows[row].Cells[4].ReadOnly = false;

            ServicesGridView.Rows[row].Cells[5].Value = "Сохранить";
        }

        private void saveRow()
        {
                //TODO: пустые поля крашат
                Service service = new Service();
                service.title = ServicesGridView.Rows[rowEditNumber].Cells[1].Value.ToString();
                service.description = ServicesGridView.Rows[rowEditNumber].Cells[1].Value.ToString();
                service.cost = Convert.ToInt32(ServicesGridView.Rows[rowEditNumber].Cells[3].Value.ToString());
                service.executionTime = Convert.ToInt32(ServicesGridView.Rows[rowEditNumber].Cells[4].Value.ToString());



                ServicesGridView.Rows[rowEditNumber].Cells[1].ReadOnly = true;
                ServicesGridView.Rows[rowEditNumber].Cells[2].ReadOnly = true;
                ServicesGridView.Rows[rowEditNumber].Cells[3].ReadOnly = true;
                ServicesGridView.Rows[rowEditNumber].Cells[4].ReadOnly = true;

                ServicesGridView.Rows[rowEditNumber].Cells[5].Value = "Редактировать";

                DataBaseController dbController = new DataBaseController();

                dbController.UpdateService(rowEditNumber, service);

                isRowEdit = false;
                rowEditNumber = -1;
      
        }

        private void deleteRow(int rowId)
        {
            DataBaseController dbController = new DataBaseController();

            dbController.DeleteService(rowId);

            loadAndDisplayServicesFromDB();
        }
    }
}
