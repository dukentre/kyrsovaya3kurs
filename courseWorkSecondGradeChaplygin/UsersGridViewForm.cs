﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class UsersGridViewForm : TemplateForm
    {

        bool isRowEdit = false;
        int rowEditNumber = -1;

        public UsersGridViewForm()
        {
            InitializeComponent();
        }

        private void UsersGridViewForm_Load(object sender, EventArgs e)
        {
            loadAndDisplayUsersFromDB();
        }

        private void loadAndDisplayUsersFromDB(string searchText = null)
        {
            DataBaseController dbController = new DataBaseController();
            List<User> users = dbController.getUsers();

            displayUsers(users,searchText);
        }
        private void displayUsers(List<User> users, string searchText = null)
        {
            UsersGridView.Rows.Clear();
            for(int i = 0; i< users.Count; i++)
            {
                if(searchText != null)
                {
                    if (!users[i].fullName.Contains(searchText))
                    {
                        continue;
                    }
                }
                UsersGridView.Rows.Add(users[i].id,users[i].login, users[i].password, users[i].fullName, User.getRoleName(users[i].userRole), users[i].isRegularCustomer,"Edit","Delete");
            }
        }

        private void UsersGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.RowIndex < 0)
            {
                return;
            }

            if(e.ColumnIndex == 6)
            {
                if (!isRowEdit)
                {
                    startRowEdit(e.RowIndex);
                }
                else
                {
                    if (rowEditNumber == e.RowIndex)
                    {
                        saveRow();
                    }
                    else
                    {
                        saveRow();
                        startRowEdit(e.RowIndex);
                    }
                }
            }
            if(e.ColumnIndex == 7)
            {
                if(MessageBox.Show("Вы уверены, что хотите удалить эти данные?","Подтверждение",MessageBoxButtons.YesNo) == DialogResult.Yes)
                    deleteRow(e.RowIndex);
            }
        }

        private void startRowEdit(int row)
        {
            isRowEdit = true;
            rowEditNumber = row;
            UsersGridView.Rows[row].Cells[1].ReadOnly = false;
            UsersGridView.Rows[row].Cells[2].ReadOnly = false;
            UsersGridView.Rows[row].Cells[3].ReadOnly = false;
            UsersGridView.Rows[row].Cells[4].ReadOnly = false;
            UsersGridView.Rows[row].Cells[5].ReadOnly = false;

            UsersGridView.Rows[row].Cells[6].Value = "Save";
        }

        private void saveRow()
        {
            User user = new User();
            user.login = UsersGridView.Rows[rowEditNumber].Cells[1].Value.ToString();
            user.password = UsersGridView.Rows[rowEditNumber].Cells[2].Value.ToString();
            user.fullName = UsersGridView.Rows[rowEditNumber].Cells[3].Value.ToString();
            user.userRole = User.getRoleByName(UsersGridView.Rows[rowEditNumber].Cells[4].Value.ToString());
            user.isRegularCustomer = Convert.ToBoolean(UsersGridView.Rows[rowEditNumber].Cells[5].Value);

            
            UsersGridView.Rows[rowEditNumber].Cells[1].ReadOnly = true;
            UsersGridView.Rows[rowEditNumber].Cells[2].ReadOnly = true;
            UsersGridView.Rows[rowEditNumber].Cells[3].ReadOnly = true;
            UsersGridView.Rows[rowEditNumber].Cells[4].ReadOnly = true;
            UsersGridView.Rows[rowEditNumber].Cells[5].ReadOnly = true;

            UsersGridView.Rows[rowEditNumber].Cells[6].Value = "Edit";

            DataBaseController dbController = new DataBaseController();

            dbController.UpdateUser(rowEditNumber, user);

            isRowEdit = false;
            rowEditNumber = -1;
        }

        private void deleteRow(int rowId)
        {
            DataBaseController dbController = new DataBaseController();

            dbController.DeleteUser(rowId);

            loadAndDisplayUsersFromDB();
        }

        private void SearchInput_TextChanged(object sender, EventArgs e)
        {
            loadAndDisplayUsersFromDB(SearchInput.Text);
        }
    }
}
