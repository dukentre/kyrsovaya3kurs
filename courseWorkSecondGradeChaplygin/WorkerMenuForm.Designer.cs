﻿
namespace courseWorkSecondGradeChaplygin
{
    partial class WorkerMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RequestsButton = new System.Windows.Forms.Button();
            this.ServicesButton = new System.Windows.Forms.Button();
            this.UsersButton = new System.Windows.Forms.Button();
            this.CreateUserButton = new System.Windows.Forms.Button();
            this.CreateServiceButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RequestsButton
            // 
            this.RequestsButton.Location = new System.Drawing.Point(29, 50);
            this.RequestsButton.Name = "RequestsButton";
            this.RequestsButton.Size = new System.Drawing.Size(140, 62);
            this.RequestsButton.TabIndex = 1;
            this.RequestsButton.Text = "Заявки";
            this.RequestsButton.UseVisualStyleBackColor = true;
            this.RequestsButton.Click += new System.EventHandler(this.RequestsButton_Click);
            // 
            // ServicesButton
            // 
            this.ServicesButton.Location = new System.Drawing.Point(29, 217);
            this.ServicesButton.Name = "ServicesButton";
            this.ServicesButton.Size = new System.Drawing.Size(140, 62);
            this.ServicesButton.TabIndex = 2;
            this.ServicesButton.Text = "Услуги";
            this.ServicesButton.UseVisualStyleBackColor = true;
            this.ServicesButton.Click += new System.EventHandler(this.ServicesButton_Click);
            // 
            // UsersButton
            // 
            this.UsersButton.Location = new System.Drawing.Point(33, 366);
            this.UsersButton.Name = "UsersButton";
            this.UsersButton.Size = new System.Drawing.Size(140, 62);
            this.UsersButton.TabIndex = 3;
            this.UsersButton.Text = "Пользователи";
            this.UsersButton.UseVisualStyleBackColor = true;
            this.UsersButton.Click += new System.EventHandler(this.UsersButton_Click);
            // 
            // CreateUserButton
            // 
            this.CreateUserButton.Location = new System.Drawing.Point(208, 366);
            this.CreateUserButton.Name = "CreateUserButton";
            this.CreateUserButton.Size = new System.Drawing.Size(140, 62);
            this.CreateUserButton.TabIndex = 4;
            this.CreateUserButton.Text = "Создать пользователя";
            this.CreateUserButton.UseVisualStyleBackColor = true;
            this.CreateUserButton.Click += new System.EventHandler(this.CreateUserButton_Click);
            // 
            // CreateServiceButton
            // 
            this.CreateServiceButton.Location = new System.Drawing.Point(208, 217);
            this.CreateServiceButton.Name = "CreateServiceButton";
            this.CreateServiceButton.Size = new System.Drawing.Size(140, 62);
            this.CreateServiceButton.TabIndex = 5;
            this.CreateServiceButton.Text = "Создать услугу";
            this.CreateServiceButton.UseVisualStyleBackColor = true;
            this.CreateServiceButton.Click += new System.EventHandler(this.CreateServiceButton_Click);
            // 
            // WorkerMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 529);
            this.Controls.Add(this.CreateServiceButton);
            this.Controls.Add(this.CreateUserButton);
            this.Controls.Add(this.UsersButton);
            this.Controls.Add(this.ServicesButton);
            this.Controls.Add(this.RequestsButton);
            this.Name = "WorkerMenuForm";
            this.Text = "WorkerMenuForm";
            this.Load += new System.EventHandler(this.WorkerMenuForm_Load);
            this.Controls.SetChildIndex(this.RequestsButton, 0);
            this.Controls.SetChildIndex(this.ServicesButton, 0);
            this.Controls.SetChildIndex(this.UsersButton, 0);
            this.Controls.SetChildIndex(this.CreateUserButton, 0);
            this.Controls.SetChildIndex(this.CreateServiceButton, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RequestsButton;
        private System.Windows.Forms.Button ServicesButton;
        private System.Windows.Forms.Button UsersButton;
        private System.Windows.Forms.Button CreateUserButton;
        private System.Windows.Forms.Button CreateServiceButton;
    }
}