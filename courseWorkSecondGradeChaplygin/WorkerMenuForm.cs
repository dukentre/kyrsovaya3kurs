﻿using courseWorkSecondGradeChaplygin.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseWorkSecondGradeChaplygin
{
    public partial class WorkerMenuForm : TemplateForm
    {
        public WorkerMenuForm()
        {
            InitializeComponent();
            ProgramState pState = new ProgramState();
            User authorizedUser = pState.GetAuthorizedUser();
            if(authorizedUser.userRole == User.role.master)
            {
                ServicesButton.Enabled = false;
                ServicesButton.Visible = false;
                UsersButton.Enabled = false;
                UsersButton.Visible = false;
                CreateUserButton.Enabled = false;
                CreateUserButton.Visible = false;
            }
        }

        private void WorkerMenuForm_Load(object sender, EventArgs e)
        {

        }

        private void UsersButton_Click(object sender, EventArgs e)
        {
            UsersGridViewForm form = new UsersGridViewForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }

        private void CreateUserButton_Click(object sender, EventArgs e)
        {
            AdminUserRegistrationForm form = new AdminUserRegistrationForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }

        private void ServicesButton_Click(object sender, EventArgs e)
        {
            ServicesGridViewForm form = new ServicesGridViewForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }

        private void CreateServiceButton_Click(object sender, EventArgs e)
        {
            AddServiceForm form = new AddServiceForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }

        private void RequestsButton_Click(object sender, EventArgs e)
        {
            OrdersGridViewForm form = new OrdersGridViewForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }
    }
}
